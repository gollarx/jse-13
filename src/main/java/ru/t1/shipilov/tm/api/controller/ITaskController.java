package ru.t1.shipilov.tm.api.controller;

public interface ITaskController {

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void clearTasks();

    void completeTaskById();

    void completeTaskByIndex();

    void createTask();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void showTasks();

    void startTaskById();

    void startTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}
