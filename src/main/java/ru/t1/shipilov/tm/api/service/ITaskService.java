package ru.t1.shipilov.tm.api.service;

import ru.t1.shipilov.tm.api.repository.ITaskRepository;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
