package ru.t1.shipilov.tm.api.repository;

import ru.t1.shipilov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
