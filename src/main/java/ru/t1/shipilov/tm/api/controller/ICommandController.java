package ru.t1.shipilov.tm.api.controller;

public interface ICommandController {

    void showErrorArgument();

    void showSystemInfo();

    void showErrorCommand();

    void showVersion();

    void showAbout();

    void showHelp();

}
