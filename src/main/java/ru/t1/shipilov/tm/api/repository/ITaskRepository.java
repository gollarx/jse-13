package ru.t1.shipilov.tm.api.repository;

import ru.t1.shipilov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    Task create(String name, String description);

    Task create(String name);

    boolean existById(String id);

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    int getSize();

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
